package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    private boolean colision;
    private boolean levelCompleted;
    private List<Car> cars = new LinkedList<>();
    private Player player;


    public Model() {

        this.player = new Player(WIDTH / 2, HEIGHT - 25);


        Random r = new Random();
        int rangeMinX = 1;
        int rangeMaxX = WIDTH;
        int rangeMinY = HEIGHT - 560;
        int rangeMaxY = HEIGHT - 100;
        float rangeMinSpeed = 0.1f;
        float rangeMaxSpeed = 0.3f;


        for (int i = 0; i < 7; i++) {
            int randomX = r.nextInt(rangeMaxX - rangeMinX) + rangeMinX;
            int randomY = r.nextInt(rangeMaxY - rangeMinY) + rangeMinY;
            float randomSpeed = rangeMinSpeed + r.nextFloat() * (rangeMaxSpeed - rangeMinSpeed);
            this.cars.add(new Car(1, randomY, randomSpeed));
        }

    }

    public void movePlayer(int dx, int dy) {
        if (!colision && !levelCompleted) {
            player.move(dx, dy);
        }
    }

    public boolean isColision() {
        return colision;
    }

    public boolean levelCompleted() {
        return levelCompleted;
    }

    public void reset() {
        colision = false;
        levelCompleted = false;
    }

    public List<Car> getcars() {
        return cars;
    }

    public Player getPlayer() {
        return player;
    }

    public void update(long elapsedTime) {
        for (Car car : cars) {
            car.update(elapsedTime);


        }
        for (Car car : cars) {
            //|X1 - x2| <= w1/2 + w2/2
            //|Y1 – y2| <= h1/2 + h2/2
            int dx = Math.abs(player.getX() - car.getX());
            int dy = Math.abs(player.getY() - car.getY());
            int w = player.getW() / 2 + car.getW() / 2;
            int h = player.getH() / 2 + car.getH() / 2;


            if (dx <= w && dy <= h) {
                this.colision = true;
                player.moveTo(WIDTH / 2, HEIGHT - getPlayer().getH() / 2);

            }
        }

        if (player.getY() <= 0 + player.getH() / 2) {
            this.levelCompleted = true;
            player.moveTo(WIDTH / 2, HEIGHT - getPlayer().getH() / 2);

        }
    }
}
