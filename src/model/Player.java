package model;

public class Player {
    private int x;
    private int y;


    public Player(int x, int y) {
        this.x = x;
        this.y = y;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return 50;
    }

    public int getH() {
        return 50;
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //Frog moves between only the pane
    public void move(int dx, int dy) {

        this.x += dx;
        this.y += dy;

        if (this.x < getW() / 2) {
            this.x = getW() / 2;
        }
        if (this.x > Model.WIDTH - getW() / 2) {
            this.x = Model.WIDTH - getW() / 2;
        }
        if (this.y < getH() / 2) {
            this.y = getH() / 2;
        }
        if (this.y > Model.HEIGHT - getH() / 2) {
            this.y = Model.HEIGHT - getH() / 2;
        }


    }


}
