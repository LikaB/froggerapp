import javafx.scene.input.KeyCode;
import model.Model;


public class InputHandler {
    private Model model;


    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.UP) {
            model.movePlayer(0, -20);
        } else if (key == KeyCode.DOWN) {
            model.movePlayer(0, 20);
        } else if (key == KeyCode.RIGHT) {
            model.movePlayer(20, 0);
        } else if (key == KeyCode.LEFT) {
            model.movePlayer(-20, 0);
        } else if (key == KeyCode.SPACE) {
            model.reset();
        }

    }

    public void onKeyReleased(KeyCode key) {


    }

}

