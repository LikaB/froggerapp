import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import model.Car;
import model.Model;

public class Graphics {
    Image iconFrog = new Image("frog.png");
    Image iconCar = new Image("car.png");
    Image gameOver = new Image("game-over.png");
    Image win = new Image("win.png");


    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {

        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.closePath();
        gc.fillRect(0, 0, 800, 600);
        gc.setFill(Color.GREY);

        //gc.setFill(Color.BLUE);
        for (Car car : this.model.getcars()) {
            gc.drawImage(iconCar, car.getX() - 40, car.getY() - 40, 80, 80);
        }
        //gc.setFill(Color.GREEN);
        gc.drawImage(iconFrog, model.getPlayer().getX() - model.getPlayer().getW() / 2,
                model.getPlayer().getY() - model.getPlayer().getH() / 2,
                model.getPlayer().getW(), model.getPlayer().getH());

        if (model.isColision()) {
            gc.drawImage(gameOver, 336, 236, 128, 128);
        }

        if (model.levelCompleted()) {
            gc.drawImage(win, 336, 236, 128, 128);
        }


    }
}

